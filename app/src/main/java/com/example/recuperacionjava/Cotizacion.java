package com.example.recuperacionjava;

public class Cotizacion {
    double Enganche;
    double CostoFinal;
    double PagoMensual;

    public Cotizacion(){
        this.Enganche = 0.0;
        this.CostoFinal = 0.0;
        this.PagoMensual = 0;
    }

    public double generarEnganche(double porcentajePagoInicial, double costoTotal){

        this.Enganche = costoTotal * porcentajePagoInicial;

        this.CostoFinal = costoTotal - this.Enganche;

        return this.Enganche;

    }

    public double generarMensualidad(int meses){
        this.PagoMensual = this.CostoFinal / meses;

        return PagoMensual;
    }

}