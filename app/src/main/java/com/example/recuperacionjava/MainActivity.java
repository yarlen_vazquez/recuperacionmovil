package com.example.recuperacionjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        }));

    }
    private void iniciarComponentes(){
        btnEntrar = findViewById(R.id.btnEntrar);
        txtNombre = findViewById(R.id.txtNombre);
    }
    private void ingresar(){
        if (txtNombre.getText().toString().isEmpty()){
            Toast.makeText(this.getApplicationContext(),"Nombre requerido",Toast.LENGTH_SHORT).show();
            return;
        }
        String strNombre = txtNombre.getText().toString();
        Intent intent = new Intent(MainActivity.this,CotizacionActivity.class);
        intent.putExtra("strNombre", strNombre);
        startActivity(intent);
    }


}